[TOC]

**解决方案介绍**
===============
该解决方案基于华为云Web应用防火墙WAF构建，可以帮助企业网站业务流量进行多维度检测和防护，全面避免网站被黑客恶意攻击和入侵。适用于如下场景：
- 银行系统/金融机构、政府/事业单位、医疗、高校、科研领域网站防御场景;
- 满足Web应用系统，等保合规、保障网站业务安全的场景;
- 防应用层攻击导致业务中断、数据泄密等场景;
- 有效应对活动特殊时期激增的业务流量和恶意流量攻击的场景。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/website-security.html

**架构图**
---------------
![方案架构](./document/huaweicloud-solution-website-security.png)

**架构描述**
---------------
该解决方案会部署如下资源：
- 创建一台云模式入门版Web应用防火墙，用于对HTTP请求进行检测，保证Web服务安全稳定。
- 在Web应用防火墙WAF上一键添加防御域名，提高配置域名的便利性，构建风险全面可控的网站安全架构，保障网站业务连续可用。


**组织结构**
---------------

``` lua

huaweicloud-solution-website-security
├──website-security.tf.json -- 资源编排模板
```
**准备工作**
---------------

**创建WAF实例**

1.登录[Web应用防火墙WAF](https://console.huaweicloud.com/waf/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/waf/dashboard)控制台，单击“创建WAF实例”。
图1 创建WAF实例

![创建WAF实例](./document/readme-image-001.png)

2.选择云模式中的入门版，单击“立即购买”。

图2 购买WAF

![创建WAF实例](./document/readme-image-002.png)

**检查源站**

1.登录[华为云控制台](https://console.huaweicloud.com/console/?locale=zh-cn&region=cn-north-4#/home)云控制台，选择源站所在的区域，本文以“北京四”区域为例。

图3 选择Region

![选择Region](./document/readme-image-003.png)

2.查看源站服务器的弹性公网IP。在左侧导航栏，选择“弹性云服务器ECS"，在华为云控制台查看源站服务器的弹性公网IP地址。

图4 查看源站服务器的弹性公网IP

![查看源站服务器的弹性公网IP](./document/readme-image-004.png)

3.在浏览器栏输入“http://EIP/ ”访问预置页面，验证源站能否正常访问。

**开始使用**
---------------
**配置WAF**

1.登录[Web应用防火墙WAF](https://console.huaweicloud.com/waf/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/waf/dashboard)控制台，进入“网站设置”页面，单击回源IP加白，按照操作步骤完成配置。

图1 网站设置

![网站设置](./document/readme-image-005.png)

2.域名接入进度变为已接入，说明配置成功。

图2 配置成功

![配置成功](./document/readme-image-006.png)

**访问测试**

1.清理浏览器缓存。

2.在本地电脑的浏览器中输入防护域名，测试网站域名是否能正常访问。

3.若站点能够正常打开，说明通过Web网站基础安全防护访问Web源站的线路连通性正常。



